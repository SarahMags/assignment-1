import java.util.Scanner; 

public class Assignment1 {
    public static void main(String[] args){
        boolean loopInput = true;

        while(loopInput == true){ //While loop to keep asking for input after making rectangle
            Scanner myScanner = new Scanner(System.in); //Creating the Scanner object
            int width;
            int height;

            do{
                System.out.println("Enter rectangle width"); //Checking if width input is a valid number bigger than 0
                while(!myScanner.hasNextInt()){ 
                    System.out.println("Please enter a valid number.");
                    myScanner.next(); //keeps the scanner going until it has the next valid int
                }
                width = myScanner.nextInt();
            } while(width < 0);

            do{
                System.out.println("Enter rectangle height"); //Checking if height input is a valid number bigger than 0
                while(!myScanner.hasNextInt()){
                    System.out.println("Please enter a valid number.");
                    myScanner.next();
                }
                height = myScanner.nextInt();
            } while(height < 0);

            for(int i = 0; i < height; i++){ //Using height value to create horizontal lines one by one
                if(i == 0 || i == height-1){ //If top or bottom line, write "#" for entire width
                    for(int j = 0; j < width; j++){
                        System.out.print("#");
                    }
                    System.out.println(); //Break line to begin making second row
                }
                else{  
                    for(int k = 0; k < width; k++){
                        if(k == 0){
                            System.out.print("#"); //Start row with "#"
                        }
                        else if (k == width-1){
                            System.out.println("#"); //Break line if "#" is at right edge
                        }
                        else{
                            System.out.print(" "); //When not at edges, print space
                        }
                    }
                }
            }
            myScanner.nextLine(); //Using nextLine to prevent skipping past next input
            System.out.println("Enter q to quit, any other letter to continue");
            String exitLoop = myScanner.nextLine();
            if(exitLoop.equals("q")){
                loopInput = false; //Exit while loop
            }
        }

        System.out.println("Thanks for playing");

    }
}